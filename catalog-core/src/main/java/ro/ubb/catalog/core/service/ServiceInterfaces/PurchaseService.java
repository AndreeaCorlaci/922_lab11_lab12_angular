package ro.ubb.catalog.core.service.ServiceInterfaces;


import ro.ubb.catalog.core.model.Exceptions.ValidatorException;
import ro.ubb.catalog.core.model.Purchase;

import java.util.List;

public interface PurchaseService {
    Purchase savePurchase(Purchase p) throws ValidatorException;
    Purchase updatePurchase(Purchase p) throws ValidatorException;
    void deletePurchase(Long idClient, Long idBook);
    List<Purchase>getAllPurchases();
    Long findPurchase(Long idClient, Long idBook);
    Purchase getPurchase(Long id);
    List<Purchase> getSortedPurchases();
}
