package ro.ubb.catalog.core.repository;


import ro.ubb.catalog.core.model.Purchase;


public interface PurchaseRepository extends CatalogRepository<Purchase,Long> {
}
