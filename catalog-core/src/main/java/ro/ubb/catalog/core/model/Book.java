package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Book extends BaseEntity<Long> {
    private String title;
    private String author;
    private int price;

    @Override
    public String toString() {
        return "Book{" +
                "title= " + title+ '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }

}