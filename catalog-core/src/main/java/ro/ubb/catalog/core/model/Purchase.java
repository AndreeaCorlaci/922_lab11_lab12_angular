package ro.ubb.catalog.core.model;

import lombok.*;

import javax.persistence.Entity;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class Purchase extends BaseEntity<Long> {
    private Long idClient;
    private Long idBook;
    private Date date;

    @Override
    public String toString() {
        return "Purchase{" +
                "idClient='" + idClient + '\'' +
                ", idBook='" + idBook + '\'' +
                ", date=" + date +
                "} " + super.toString();
    }
}
