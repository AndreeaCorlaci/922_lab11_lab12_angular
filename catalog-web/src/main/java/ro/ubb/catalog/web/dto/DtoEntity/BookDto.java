package ro.ubb.catalog.web.dto.DtoEntity;

import lombok.*;
import ro.ubb.catalog.web.dto.BaseDto;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class BookDto extends BaseDto {
    private String title;
    private String author;
    private int price;

    @Override
    public String toString() {
        return "BookDto{" +
                "title= " + title+ '\'' +
                ", author='" + author + '\'' +
                ", price=" + price +
                "} " + super.toString();
    }

}
