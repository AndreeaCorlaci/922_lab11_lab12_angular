package ro.ubb.catalog.web.dto.DtoEntity;

import lombok.*;
import ro.ubb.catalog.web.dto.BaseDto;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ClientDto extends BaseDto {
    private String name;

    @Override
    public String toString() {
        return "ClientDto{" +
                "name='" + name + '\'' +  "} " + super.toString();
    }
}
