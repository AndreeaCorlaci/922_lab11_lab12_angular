package ro.ubb.catalog.web.dto.DtoEntity;

import lombok.*;
import ro.ubb.catalog.web.dto.BaseDto;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PurchaseDto extends BaseDto {
    private Long idClient;
    private Long idBook;
    private Date date;

    @Override
    public String toString() {
        return "PurchaseDto{" +
                "idClient='" + idClient + '\'' +
                ", idBook='" + idBook + '\'' +
                ", date=" + date +
                "} " + super.toString();
    }
}
