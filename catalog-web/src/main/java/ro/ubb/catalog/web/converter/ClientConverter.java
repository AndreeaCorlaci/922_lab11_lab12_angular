package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Client;
import ro.ubb.catalog.web.dto.DtoEntity.ClientDto;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto> {

    private static final Logger log = LoggerFactory.getLogger(ClientConverter.class);

    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = new Client(dto.getName());
        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto dto = new ClientDto (client.getName());
        dto.setId(client.getId());
        return dto;
    }
}
