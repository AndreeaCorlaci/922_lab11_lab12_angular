package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Purchase;
import ro.ubb.catalog.web.dto.DtoEntity.PurchaseDto;

@Component
public class PurchaseConverter extends BaseConverter<Purchase, PurchaseDto> {

    private static final Logger log = LoggerFactory.getLogger(PurchaseConverter.class);

    @Override
    public Purchase convertDtoToModel(PurchaseDto dto) {
        Purchase purchase = new  Purchase(dto.getIdBook(), dto.getIdClient(), dto.getDate());
        purchase.setId(dto.getId());
        return purchase;
    }

    @Override
    public PurchaseDto convertModelToDto(Purchase p) {
        PurchaseDto dto = new PurchaseDto(p.getIdBook(), p.getIdClient(), p.getDate());
        dto.setId(p.getId());
        return dto;
    }
}
