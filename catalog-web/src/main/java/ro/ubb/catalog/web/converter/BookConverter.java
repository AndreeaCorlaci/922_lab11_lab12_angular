package ro.ubb.catalog.web.converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import ro.ubb.catalog.core.model.Book;
import ro.ubb.catalog.web.dto.DtoEntity.BookDto;

@Component
public class BookConverter extends BaseConverter<Book,BookDto> {
    private static final Logger log = LoggerFactory.getLogger(BookConverter.class);

    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = new Book(dto.getTitle(),dto.getAuthor(),dto.getPrice());
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = new BookDto(book.getTitle(),book.getAuthor(), book.getPrice());
        dto.setId(book.getId());
        return dto;
    }
}
